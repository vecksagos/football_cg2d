# Football cg
## Como compilar:
- Entre no diretório e use os comandos:
```shell
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Debug ../
```
- Se quiser usar o clang como compilador, execute
```shell
CXX=clang++ cmake -DCMAKE_BUILD_TYPE=Debug ../
```

## Gerando a documentação:
- Depois de rodar o cmake, execute:
```shell
make doc
```

## Rodando:
- Para rodar o programa, use:
```shell
./footballcode
```
## Testes:
- Para rodar os testes, use:
```shell
ctest -V
```
