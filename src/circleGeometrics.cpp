#include "circleGeometrics.h"
#include "geometryAlgorithm.h"

using namespace std;

CircleGeometrics::CircleGeometrics(const std::pair<int,int> &center,
                                   unsigned radius, int color, float thickness,
                                   bool algorithm)
  : BasicGeometrics(color, thickness, algorithm) {
  auto v = GeometryAlgorithm::chooser(algorithm).get()->drawCircle(center, radius);

  setPoints(v);
}

void CircleGeometrics::load() {
}

void CircleGeometrics::update() {

}
