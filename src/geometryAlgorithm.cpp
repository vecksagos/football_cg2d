#include "geometryAlgorithm.h"
#include "simpleAlgorithm.h"
#include "bresenhamAlgorithm.h"

using namespace std;

unique_ptr<GeometryAlgorithm> GeometryAlgorithm::chooser(int algorithm) {
  if (algorithm)
    return make_unique<SimpleAlgorithm>();
  else
    return make_unique<BresenhamAlgorithm>();
}
