#include "aux.h"
#include <cmath>

using namespace std;

namespace aux {
  template<typename T>
  void SWAP(T &a, T &b) {
    a ^= b;
    b ^= a;
    a ^= b;
  }

  template<typename T>
  void SWAP(T *a, T *b) {
    a ^= b;
    b ^= a;
    a ^= b;
  }

  unsigned distance(const pair<int,int> &a, const pair<int,int> &b) {
    return static_cast<unsigned>(sqrt((b.first - a.first)*(b.first - a.first) +
                                      (b.second - a.second)*(b.second - a.second)));
  }
}
