/**
   @mainpage
   @brief
   Football-CG is a project to computer graphics at UFAL with C++, QT and
   OpenGL.

   The goal here is to draw a football(soccer) field, and the user draws the
   rest of the stadium.
   @author Bruno da Silva Belo and Leonildo
   @see https://github.com/BrunodaSilvaBelo/football_cg2d
   @see http://www.qt.io/
   @see https://www.opengl.org/
*/
#include <stdio.h>
#include <QApplication>
#include "mainWindow.h"

using namespace std;

int main(int argc, char **argv) {
  QApplication app(argc, argv);

  MainWindow ui;
  ui.show();

  return app.exec();
}
