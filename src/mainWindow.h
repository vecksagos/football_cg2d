#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <utility>

class GLWidget;
class QLabel;
class QRadioButton;
class QSpinBox;
class QComboBox;
class QPushButton;

class MainWindow : public QWidget {
  Q_OBJECT
public:
  MainWindow();
  ~MainWindow();
  void render();
  void update();

public slots:
  void getEntity(const std::pair<int, int> &initial,
               const std::pair<int, int> &final);

private:
  GLWidget *glWidget = nullptr;
  QRadioButton *bresenhamCheckbox = nullptr;
  QRadioButton *simpleCheckbox = nullptr;
  QPushButton *quitButton = nullptr;
  QLabel *label_3 = nullptr;
  QLabel *label_4 = nullptr;
  QSpinBox *thicknessSpinbox = nullptr;
  QComboBox *colorCombobox = nullptr;
  QLabel *label = nullptr;
  QPushButton *undoButton = nullptr;
  QComboBox *entityCombobox = nullptr;
  QLabel *label_2 = nullptr;

  enum Form : unsigned { LINE, CIRCLE, RECT, SOCCERFIELD };
};

#endif /* MAINWINDOW_H */
