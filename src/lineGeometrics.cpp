#include "lineGeometrics.h"
#include "geometryAlgorithm.h"

using namespace std;

LineGeometrics::LineGeometrics(const std::pair<int,int> &first,
                               const std::pair<int, int> &second, int color,
                               float thickness, bool algorithm)
  : BasicGeometrics(color, thickness, algorithm) {
  auto v = GeometryAlgorithm::chooser(algorithm).get()->drawLine(first, second);

  setPoints(v);
}

void LineGeometrics::load() {

}

void LineGeometrics::update() {

}
