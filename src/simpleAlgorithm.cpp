#include "simpleAlgorithm.h"
#include "aux.h"
#include <cmath>
#include <algorithm>
#include <stdio.h>

using namespace std;

SimpleAlgorithm::~SimpleAlgorithm() {}

vector<pair<int, int>> SimpleAlgorithm::drawLine(const pair<int, int> &first,
                                                 const pair<int, int> &second) {
  return (abs(second.first - first.first) < abs(second.second - first.second))
    ? drawVerticalLine(first, second) : drawHorizontalLine(first, second);
}

vector<pair<int,int>>
  SimpleAlgorithm::drawHorizontalLine(const pair<int,int> &first,
                                      const pair<int,int> &second) {
  auto initial = first;
  auto final = second;

  float a = static_cast<float>((final.second - initial.second)) /
    static_cast<float>((final.first - initial.first));

  if (initial.first > final.first)
    initial.swap(final);

  vector<pair<int, int>> v;
  for (int x = initial.first; x <= final.first; ++x)
    v.push_back({x, initial.second + (a * (x - initial.first))});

  return v;
}

vector<pair<int,int>>
  SimpleAlgorithm::drawVerticalLine(const pair<int,int> &first,
                                    const pair<int,int> &second) {
  auto initial = first;
  auto final = second;

  initial.first ^= initial.second;
  initial.second ^= initial.first;
  initial.first ^= initial.second;

  final.first ^= final.second;
  final.second ^= final.first;
  final.first ^= final.second;

  float a = static_cast<float>((final.second - initial.second)) /
    static_cast<float>((final.first - initial.first));

  if (initial.first > final.first)
    initial.swap(final);

  vector<pair<int, int>> v;
  for (int x = initial.first; x <= final.first; ++x)
    v.push_back({initial.second + (a * (x - initial.first)), x});

  return v;
}

vector<pair<int, int>> SimpleAlgorithm::drawCircle(const pair<int, int> &center,
                                                   const unsigned int radius) {
  vector<pair<int, int>> v;
  auto eightOctante = [&v, &center](const pair<int,int> &point) {
    v.push_back({point.first + center.first,
          point.second + center.second});
    v.push_back({point.first + center.first,
          -point.second + center.second});
    v.push_back({-point.first + center.first,
          point.second + center.second});
    v.push_back({-point.first + center.first,
          -point.second + center.second});
    v.push_back({point.second + center.first,
          point.first + center.second});
    v.push_back({point.second + center.first,
          -point.first + center.second});
    v.push_back({-point.second + center.first,
          point.first + center.second});
    v.push_back({-point.second + center.first,
          -point.first + center.second});
  };
  for (int x = 0; x < static_cast<int>(radius); ++x)
    eightOctante({x,
          (sqrt(static_cast<int>(radius*radius) - (x*x)))});

  return v;
}

vector<pair<int,int>>
SimpleAlgorithm::drawRect(const std::pair<int,int> &first,
                            const std::pair<int, int> &second) {
  vector<vector<pair<int,int>>> aux;

  aux.push_back(drawLine(first, {second.first, first.second}));
  aux.push_back(drawLine(first, {first.first, second.second}));
  aux.push_back(drawLine(second, {first.first, second.second}));
  aux.push_back(drawLine(second, {second.first, first.second}));

  vector<pair<int,int>> v;
  for (auto &i : aux)
    v.insert(v.end(), i.begin(), i.end());

  return v;
}
