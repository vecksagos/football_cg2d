#ifndef AUX_H
#define AUX_H

#include <utility>

namespace aux {
  template<typename T>
  void SWAP(T &a, T &b);
  template<typename T>
  void SWAP(T *a, T *b);
  unsigned distance(const std::pair<int,int> &a, const std::pair<int,int> &b);
}

#endif /* AUX_H */
