#include "entityFactory.h"
#include "lineGeometrics.h"
#include "circleGeometrics.h"
#include "rectGeometrics.h"
#include "soccerField.h"

using namespace std;

unique_ptr<Entity>
EntityFactory::createLineGeometrics(const pair<int,int> &initial,
                                    const pair<int,int> &final, int color,
                                    float thickness, bool algorithm) {
  return
    make_unique<LineGeometrics>(initial, final, color, thickness, algorithm);
}

unique_ptr<Entity>
EntityFactory::createCircleGeometrics(const std::pair<int,int> &center,
                                      unsigned radius, int color,
                                      float thickness, bool algorithm) {
  return make_unique<CircleGeometrics>(center, radius, color, thickness,
                                       algorithm);
}

unique_ptr<Entity>
EntityFactory::createRectGeometrics(const pair<int,int> &initial,
                           const pair<int,int> &final, int color,
                           float thickness, bool algorithm) {
  return make_unique<RectGeometrics>(initial, final, color, thickness, algorithm);
}

unique_ptr<Entity>
EntityFactory::createSoccerField(const pair<int,int> &initial,
                           const pair<int,int> &final, int color,
                           float thickness, bool algorithm) {
  return make_unique<SoccerField>(initial, final, color, thickness, algorithm);
}
