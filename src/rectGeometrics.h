#ifndef RECTGEOMETRICS_H
#define RECTGEOMETRICS_H

#include "basicGeometrics.h"

class RectGeometrics : public BasicGeometrics {
public:
  RectGeometrics(const std::pair<int,int> &first,
                 const std::pair<int, int> &second, int color, float thickness,
                 bool algorithm);
  virtual void load();
  virtual void update();
};

#endif /* RECTGEOMETRICS_H */
