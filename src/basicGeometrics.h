#ifndef BASICGEOMETRICS_H
#define BASICGEOMETRICS_H

#include <vector>
#include <utility>
#include "entity.h"

class BasicGeometrics : public Entity {
public:
  BasicGeometrics(int color, float thickness, bool algorithm);
  virtual ~BasicGeometrics() {}
  virtual void load() = 0;
  virtual void render() const;
  virtual void update() = 0;

protected:
  void setPoints(std::vector<std::pair<int,int>> &other);

private:
  bool algorithm;
  int color;
  float thickness;
  std::vector<std::pair<int,int>> points;

  void colorGL() const ;
};

#endif /* BASICGEOMETRICS_H */
