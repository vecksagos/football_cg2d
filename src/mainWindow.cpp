#include "mainWindow.h"
#include "glWidget.h"
#include <QOpenGLWidget>
#include <QLabel>
#include <QSpinBox>
#include <QComboBox>
#include <QRadioButton>
#include <QPushButton>
#include <QApplication>
#include <memory>
#include "entityFactory.h"
#include <cmath>
#include "aux.h"

using namespace std;

MainWindow::MainWindow() {
  if (objectName().isEmpty())
    setObjectName(QStringLiteral("MainWindow"));
  resize(800, 600);
  setMinimumSize(QSize(800, 600));
  setMaximumSize(QSize(800, 600));
  glWidget = new GLWidget(this);
  glWidget->setObjectName(QStringLiteral("glWidget"));
  glWidget->setGeometry(QRect(0, 0, 600, 600));
  bresenhamCheckbox = new QRadioButton(this);
  bresenhamCheckbox->setObjectName(QStringLiteral("bresenhamCheckbox"));
  bresenhamCheckbox->setGeometry(QRect(610, 70, 102, 20));
  bresenhamCheckbox->setChecked(false);
  simpleCheckbox = new QRadioButton(this);
  simpleCheckbox->setObjectName(QStringLiteral("simpleCheckbox"));
  simpleCheckbox->setGeometry(QRect(610, 40, 102, 20));
  simpleCheckbox->setChecked(true);
  quitButton = new QPushButton(this);
  quitButton->setObjectName(QStringLiteral("quitButton"));
  quitButton->setGeometry(QRect(660, 550, 81, 22));
  label_3 = new QLabel(this);
  label_3->setObjectName(QStringLiteral("label_3"));
  label_3->setGeometry(QRect(610, 150, 59, 14));
  label_4 = new QLabel(this);
  label_4->setObjectName(QStringLiteral("label_4"));
  label_4->setGeometry(QRect(660, 200, 81, 16));
  thicknessSpinbox = new QSpinBox(this);
  thicknessSpinbox->setObjectName(QStringLiteral("thicknessSpinbox"));
  thicknessSpinbox->setGeometry(QRect(610, 200, 48, 23));
  thicknessSpinbox->setMinimum(1);
  thicknessSpinbox->setMaximum(100);
  thicknessSpinbox->setValue(1);
  colorCombobox = new QComboBox(this);
  colorCombobox->setObjectName(QStringLiteral("colorCombobox"));
  colorCombobox->setGeometry(QRect(610, 170, 111, 22));
  label = new QLabel(this);
  label->setObjectName(QStringLiteral("label"));
  label->setGeometry(QRect(610, 10, 59, 16));
  label->setTextFormat(Qt::PlainText);
  undoButton = new QPushButton(this);
  undoButton->setObjectName(QStringLiteral("undoButton"));
  undoButton->setGeometry(QRect(610, 230, 81, 22));
  entityCombobox = new QComboBox(this);
  entityCombobox->setObjectName(QStringLiteral("entityComboBox"));
  entityCombobox->setGeometry(QRect(610, 120, 111, 22));
  label_2 = new QLabel(this);
  label_2->setObjectName(QStringLiteral("label_2"));
  label_2->setGeometry(QRect(610, 100, 59, 14));

  setWindowTitle(QApplication::translate("MainWindow", "Form", 0));
  label_2->setText(QApplication::translate("MainWindow", "Forma", 0));
  bresenhamCheckbox->setText(QApplication::translate("MainWindow", "Bresenham", 0));
  simpleCheckbox->setText(QApplication::translate("MainWindow", "Simples", 0));
  quitButton->setText(QApplication::translate("MainWindow", "Sair", 0));
  label_3->setText(QApplication::translate("MainWindow", "Cor", 0));
  label_4->setText(QApplication::translate("MainWindow", "Tamanhos", 0));
  colorCombobox->clear();
  colorCombobox->insertItems(0, QStringList()
                             << QApplication::translate("MainWindow", "Branco", 0)
                             << QApplication::translate("MainWindow", "Vermelho", 0)
                             << QApplication::translate("MainWindow", "Verde", 0)
                             << QApplication::translate("MainWindow", "Azul", 0)
                             );
  label->setText(QApplication::translate("MainWindow", "Algoritmo", 0));
  undoButton->setText(QApplication::translate("MainWindow", "Desfazer", 0));
  entityCombobox->clear();
  entityCombobox->insertItems(0, QStringList()
                              << QApplication::translate("MainWindow", "Reta", 0)
                              << QApplication::translate("MainWindow", "Circulo", 0)
                              << QApplication::translate("MainWindow", "Retangulo", 0)
                              << QApplication::translate("MainWindow", "Campo de futebol", 0)
                              );

  QObject::connect(quitButton, SIGNAL(clicked()), this, SLOT(close()));
  QObject::connect(undoButton, SIGNAL(clicked()), glWidget, SLOT(undo()));
  QMetaObject::connectSlotsByName(this);
}

MainWindow::~MainWindow() {
  delete label_2;
  delete entityCombobox;
  delete undoButton;
  delete label;
  delete colorCombobox;
  delete thicknessSpinbox;
  delete label_4;
  delete label_3;
  delete quitButton;
  delete simpleCheckbox;
  delete bresenhamCheckbox;
  delete glWidget;
}

void MainWindow::getEntity(const pair<int, int> &initial,
                           const pair<int, int> &final) {
  unique_ptr<Entity> l;
  if (entityCombobox->currentIndex() == LINE)
    l =
      EntityFactory::createLineGeometrics(initial, final,
                                          colorCombobox->currentIndex(),
                                          thicknessSpinbox->value(),
                                          simpleCheckbox->isChecked());
  else if (entityCombobox->currentIndex() == CIRCLE) {
    auto radius = aux::distance(initial, final);
    l =
      EntityFactory::createCircleGeometrics(initial, radius,
                                            colorCombobox->currentIndex(),
                                            thicknessSpinbox->value(),
                                            simpleCheckbox->isChecked());
  } else if (entityCombobox->currentIndex() == RECT) {
    l =
      EntityFactory::createRectGeometrics(initial,
                                          final,colorCombobox->currentIndex(),
                                          thicknessSpinbox->value(),
                                          simpleCheckbox->isChecked());
  } else if (entityCombobox->currentIndex() == SOCCERFIELD)
    l =
      EntityFactory::createSoccerField(initial,
                                          final,colorCombobox->currentIndex(),
                                          thicknessSpinbox->value(),
                                          simpleCheckbox->isChecked());

  glWidget->put(std::move(l));
}
