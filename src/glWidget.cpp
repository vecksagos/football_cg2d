#include "glWidget.h"
#include <QWidget>
#include <QMouseEvent>
#include <QKeyEvent>
#include <GL/glu.h>

using namespace std;

GLWidget::GLWidget(QWidget *parent) :
  QOpenGLWidget(parent) {
  QObject::connect(this, SIGNAL(createEntity(const std::pair<int,int>&,
                                           const std::pair<int,int>&)),
                   parent, SLOT(getEntity(const std::pair<int,int>&,
                                        const std::pair<int,int>&)));

  QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(update()));
  QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(m_update()));

  timer.start(SCREEN_FPS);
}

void GLWidget::put(unique_ptr<Entity> entity) {
  entities.push_back(std::move(entity));
}

void GLWidget::undo() {
  if (!entities.empty())
    entities.pop_back();
}

void GLWidget::initializeGL() {
  glClearColor(0.f, 0.f, 0.f, 1.f);
}

void GLWidget::paintGL() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  for (auto &i : entities)
    i.get()->render();
}

void GLWidget::resizeGL(int w, int h) {
  glViewport(0,0,w,h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
  points.push_back({event->x() - (width() / 2),
        (height() - event->y()) - (height() / 2)});
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
  switch(event->key()) {
  case Qt::Key_Escape: {
    close();
  } break;
  default: {
    event->ignore();
  } break;
  }
}

void GLWidget::m_update() {
  if (points.size() == 2) {
    emit createEntity(points[0], points[1]);
    points.clear();
  }
}
