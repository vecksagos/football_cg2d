#ifndef ENTITY_H
#define ENTITY_H

class Entity {
public:
  virtual ~Entity() = 0;
  virtual void load() = 0;
  virtual void render() const = 0;
  virtual void update() = 0;
};

inline Entity::~Entity() = default;

#endif /* ENTITY_H */
