#include "basicGeometrics.h"
#include <GL/gl.h>
#include "geometryAlgorithm.h"

using namespace std;

BasicGeometrics::BasicGeometrics(int color, float thickness, bool algorithm)
  : algorithm(algorithm)
  , color(color)
  , thickness(thickness) {
}

void BasicGeometrics::render() const {
  glPointSize(thickness);
  colorGL();
  glBegin(GL_POINTS);
  for (auto &i : points)
    glVertex2f(static_cast<float>(i.first) / 300.f, static_cast<float>(i.second) / 300.f);
  glEnd();
}

void BasicGeometrics::setPoints(vector<pair<int,int>> &other) {
  points = move(other);
}

void BasicGeometrics::colorGL() const {
  switch(color) {
  case 0: {
    glColor3f(1.f, 1.f, 1.f);
  } break;
  case 1: {
    glColor3f(1.f, 0.f, 0.f);
  } break;
  case 2: {
    glColor3f(0.f, 1.f, 0.f);
  } break;
  case 3: {
    glColor3f(0.f, 0.f, 1.f);
  } break;
  default: {
    glColor3f(0.f, 0.f, 0.f);
  } break;
  }
}
