/**
   @file glWidget.h
   @author Bruno da Silva Belo
   @date 19 Feb 2016

   @brief File contain the GLWidget class.

   @see http://doc.qt.io/qt-5/qopenglwidget.html
*/

#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include <utility>
#include <vector>
#include <memory>
#include "entity.h"
#include <QTimer>

class QWidget;
class QMouseEvent;
class QKeyEvent;

/**
   @class GLWidget
 */
class GLWidget : public QOpenGLWidget {
  Q_OBJECT
public:
  /**
     @brief Default constructor to Qt Objects
     @param parent A pointer to parent qt object.
  */
  explicit GLWidget(QWidget *parent = nullptr);
  /**
     @brief Take the entity and push on the render vector.
     @param entity The entity to render.
  */
  void put(std::unique_ptr<Entity> entity);

public slots:
  /**
     @brief Undo the last thing done to the window.
  */
  void undo();

signals:
  /**
     @brief Create a new entity with the points
     @param initial The first point.
     @param final The second point.
  */
  void createEntity(const std::pair<int, int> &initial,
                  const std::pair<int, int> &final);

protected:
  void initializeGL();
  void paintGL();
  void resizeGL(int w, int h);
  void mousePressEvent(QMouseEvent *event);
  void keyPressEvent(QKeyEvent *event);

private:
  static constexpr int SCREEN_FPS = 1000 / 60;
  QTimer timer;
  std::vector<std::pair<int, int>> points;
  std::vector<std::unique_ptr<Entity>> entities;

private slots:
  void m_update();
};

#endif /* GLWIDGET_H */
