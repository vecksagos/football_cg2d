/**
   @file test.cpp
   @brief This file contain the test cases of all methods
   @author Bruno da Silva Belo
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "aux.h"

using namespace std;

TEST_CASE("Test of the Auxiliar lib", "[aux]") {
  /**
      @test aux::distance with various points.
      @brief See with the distance of two points is correct calculated.
  */
  SECTION("Testing the distance function!") {
    REQUIRE(aux::distance({0,0},{0,2}) == 2);
    REQUIRE(aux::distance({0,0},{0,0}) == 0);
    REQUIRE(aux::distance({0,0},{3,4}) == 5);
    REQUIRE(aux::distance({0,0},{-2,0}) == 2);
    REQUIRE(aux::distance({0,0},{-3,-4}) == 5);
  }
}
