#ifndef CIRCLEGEOMETRICS_H
#define CIRCLEGEOMETRICS_H

#include <utility>
#include "basicGeometrics.h"

class CircleGeometrics : public BasicGeometrics {
public:
  CircleGeometrics(const std::pair<int,int> &center, unsigned radius, int color,
                   float thickness, bool algorithm);
  virtual void load();
  virtual void update();
};

#endif /* CIRCLEGEOMETRICS_H */
