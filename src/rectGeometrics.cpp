#include "rectGeometrics.h"
#include "geometryAlgorithm.h"

using namespace std;

RectGeometrics::RectGeometrics(const std::pair<int,int> &first,
                               const std::pair<int, int> &second, int color,
                               float thickness, bool algorithm)
  : BasicGeometrics(color, thickness, algorithm) {
  vector<vector<pair<int,int>>> aux;
  aux.push_back(GeometryAlgorithm::chooser(algorithm).get()->
                drawRect(first, second));
  vector<pair<int,int>> v;
  for (auto &i : aux)
    v.insert(v.end(), i.begin(), i.end());

  setPoints(v);
}

void RectGeometrics::load() {

}

void RectGeometrics::update() {

}
