#ifndef LINEGEOMETRICS_H
#define LINEGEOMETRICS_H

#include <utility>
#include "basicGeometrics.h"

class LineGeometrics : public BasicGeometrics {
public:
  LineGeometrics(const std::pair<int,int> &first,
                 const std::pair<int, int> &second, int color, float thickness,
                 bool algorithm);
  virtual void load();
  virtual void update();
};

#endif /* LINEGEOMETRICS_H */
