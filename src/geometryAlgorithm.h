#ifndef GEOMETRYALGORITHM_H
#define GEOMETRYALGORITHM_H

#include <vector>
#include <utility>
#include <memory>

class GeometryAlgorithm {
public:
  virtual ~GeometryAlgorithm() {}
  virtual std::vector<std::pair<int, int>>
    drawLine(const std::pair<int, int> &first,
             const std::pair<int, int> &second) = 0;
  virtual std::vector<std::pair<int, int>>
    drawCircle(const std::pair<int, int> &center,
               const unsigned int radius) = 0;
  virtual std::vector<std::pair<int, int>>
    drawRect(const std::pair<int, int> &first,
             const std::pair<int, int> &second) = 0;
  static std::unique_ptr<GeometryAlgorithm> chooser(int algorithm);
};

#endif /* GEOMETRYALGORITHM_H */
