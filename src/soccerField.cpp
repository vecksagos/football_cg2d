#include "soccerField.h"
#include <vector>
#include "geometryAlgorithm.h"
#include <cmath>
#include "aux.h"

using namespace std;

SoccerField::SoccerField(const std::pair<int,int> &first,
                         const std::pair<int, int> &second, int color,
                         float thickness, bool algorithm)
  : BasicGeometrics(color, thickness, algorithm) {
  auto middleX = (first.first + second.first) / 2;
  auto middleY = (first.second + second.second) / 2;

  auto alg = GeometryAlgorithm::chooser(algorithm);
  auto v = alg.get()->drawRect(first, second);
  for (auto &i : (abs(middleX - first.first) < abs(middleY - first.second))
         ? verticalField(first, second, move(alg))
         : horizontalField(first, second, move(alg)))
    v.insert(v.end(), i.begin(), i.end());

  setPoints(v);
}

void SoccerField::load() {

}

void SoccerField::update() {

}

vector<vector<pair<int,int>>>
SoccerField::horizontalField(const std::pair<int,int> &first,
                             const std::pair<int, int> &second,
                             std::unique_ptr<GeometryAlgorithm> alg) {
  auto middleX = (first.first + second.first) / 2;
  auto middleY = (first.second + second.second) / 2;

  vector<vector<pair<int,int>>> v;
  v.push_back(alg.get()->drawLine({middleX, second.second},
                                  {middleX, first.second}));
  v.push_back(alg.get()->
              drawCircle({middleX, middleY},
                         static_cast<unsigned>(abs(middleY - first.second) / 2)));
  /// Penality area
  v.push_back(alg.get()->drawRect({first.first, middleY +
          abs(first.second - middleY) / 2}, {first.first + abs(first.first - middleX) / 3,
          second.second + abs(middleY - second.second) / 2}));
  v.push_back(alg.get()->drawRect({second.first, middleY +
          abs(first.second - middleY) / 2}, {second.first - abs(middleX - second.first) / 3,
          second.second + abs(middleY - second.second) / 2}));

  v.push_back(alg.get()->drawCircle({first.first + abs(first.first - middleX) / 3,
          middleY}, static_cast<unsigned>(abs(((first.first + abs(first.first - middleX) / 3)
                                              - first.first) / 3))));
  v.push_back(alg.get()->drawCircle({second.first - abs(second.first - middleX) / 3,
          middleY}, static_cast<unsigned>(abs(((second.first + abs(second.first - middleX) / 3)
                                              - second.first) / 3))));
  return v;
}

vector<vector<pair<int,int>>>
SoccerField::verticalField(const std::pair<int,int> &first,
                           const std::pair<int, int> &second,
                           std::unique_ptr<GeometryAlgorithm> alg) {
  auto middleX = (first.first + second.first) / 2;
  auto middleY = (first.second + second.second) / 2;

  vector<vector<pair<int,int>>> v;
  v.push_back(alg.get()->drawLine({second.first, middleY},
                                  {first.first, middleY}));
  v.push_back(alg.get()->
              drawCircle({middleX, middleY},
                         static_cast<unsigned>(abs(middleX - first.first) / 2)));
  /// Penality area
  v.push_back(alg.get()->drawRect({middleX +
          abs(second.first - middleX) / 2, second.second}, {first.first + abs(middleX - first.first) / 2,
          second.second + abs(middleY - second.second) / 3}));
  v.push_back(alg.get()->drawRect({middleX +
          abs(first.first - middleX) / 2, first.second}, {middleX - abs(middleX - first.first) / 2,
          first.second - abs(middleY - first.second) / 3}));

  v.push_back(alg.get()->drawCircle({middleX, second.second + abs(second.second - middleY) / 3},
                                    static_cast<unsigned>(abs(((second.second + abs(second.second - middleY) / 3)
                                                               - second.second) / 3))));
  v.push_back(alg.get()->drawCircle({middleX, first.second - abs(first.second - middleY) / 3},
                                    static_cast<unsigned>(abs(((first.second + abs(first.second - middleY) / 3)
                                              - first.second) / 3))));

  return v;
}
