#include "bresenhamAlgorithm.h"

using namespace std;

BresenhamAlgorithm::~BresenhamAlgorithm() {}

vector<pair<int, int>> BresenhamAlgorithm::drawLine(const pair<int, int> &first,
                                                    const pair<int, int> &second) {
  return (abs(second.first - first.first) < abs(second.second - first.second))
    ? drawVerticalLine(first, second) : drawHorizontalLine(first, second);
}

vector<pair<int, int>>
  BresenhamAlgorithm::drawCircle(const pair<int, int> &center,
                                 const unsigned int radius) {
  vector<pair<int, int>> v;
  auto eightOctante = [&v, &center](const pair<int,int> &point) {
    v.push_back({point.first + center.first,
          point.second + center.second});
    v.push_back({point.first + center.first,
          -point.second + center.second});
    v.push_back({-point.first + center.first,
          point.second + center.second});
    v.push_back({-point.first + center.first,
          -point.second + center.second});
    v.push_back({point.second + center.first,
          point.first + center.second});
    v.push_back({point.second + center.first,
          -point.first + center.second});
    v.push_back({-point.second + center.first,
          point.first + center.second});
    v.push_back({-point.second + center.first,
          -point.first + center.second});
  };

  int d = 1 - static_cast<int>(radius);
  int x = 0;
  int y = static_cast<int>(radius);

  while (x < y) {
    eightOctante({x, y});
    if (d < 0) {
      d += 2 * x + 3;
    } else {
      d += 2 * (x - y) + 5;
      --y;
    }
    ++x;
  }
  return v;
}

vector<std::pair<int, int>>
  BresenhamAlgorithm::drawRect(const std::pair<int, int> &first,
                               const std::pair<int, int> &second) {
  vector<vector<pair<int,int>>> aux;

  aux.push_back(drawLine(first, {second.first, first.second}));
  aux.push_back(drawLine(first, {first.first, second.second}));
  aux.push_back(drawLine(second, {first.first, second.second}));
  aux.push_back(drawLine(second, {second.first, first.second}));

  vector<pair<int,int>> v;
  for (auto &i : aux)
    v.insert(v.end(), i.begin(), i.end());

  return v;
}

vector<pair<int, int>>
  BresenhamAlgorithm::drawVerticalLine(const std::pair<int, int> &first,
                                       const std::pair<int, int> &second) {
  auto initial = first;
  auto final = second;

  initial.first ^= initial.second;
  initial.second ^= initial.first;
  initial.first ^= initial.second;

  final.first ^= final.second;
  final.second ^= final.first;
  final.first ^= final.second;

  if (initial.first > final.first)
    initial.swap(final);

  int dx = final.first - initial.first;
  int dy = final.second - initial.second;

  int slope;
  if (dy < 0) {
    slope = -1;
    dy = -dy;
  } else
    slope = 1;

  int d = (2 * dy) - dx;
  int x = initial.first;
  int y = initial.second;
  int ne = 2 * (dy - dx);
  int e = 2 * dy;

  vector<pair<int, int>> v = {{y, x}};
  while(x <= final.first) {
    if (d < 0) {
      d = d + e;
      ++x;
    } else {
      d = d + ne;
      ++x;
      y += slope;
    }
    v.push_back({y, x});
  }

  return v;
}

vector<pair<int, int>>
  BresenhamAlgorithm::drawHorizontalLine(const std::pair<int, int> &first,
                                         const std::pair<int, int> &second) {
  auto initial = first;
  auto final = second;

  if (initial.first > final.first)
    initial.swap(final);

  int dx = final.first - initial.first;
  int dy = final.second - initial.second;

  int slope;
  if (dy < 0) {
    slope = -1;
    dy = -dy;
  } else
    slope = 1;

  int d = (2 * dy) - dx;
  int x = initial.first;
  int y = initial.second;
  int ne = 2 * (dy - dx);
  int e = 2 * dy;

  vector<pair<int, int>> v = {{x, y}};
  while(x <= final.first) {
    if (d < 0) {
      d = d + e;
      ++x;
    } else {
      d = d + ne;
      ++x;
      y += slope;
    }
    v.push_back({x, y});
  }

  return v;
}
