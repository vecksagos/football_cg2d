#ifndef ENTITYFACTORY_H
#define ENTITYFACTORY_H

#include <memory>
#include <utility>
#include "entity.h"

class EntityFactory {
public:
  static std::unique_ptr<Entity>
  createLineGeometrics(const std::pair<int,int> &initial,
                       const std::pair<int,int> &final, int color,
                       float thickness, bool algorithm);
  static std::unique_ptr<Entity>
  createCircleGeometrics(const std::pair<int,int> &center, unsigned radius,
                       int color, float thickness, bool algorithm);
  static std::unique_ptr<Entity>
  createRectGeometrics(const std::pair<int,int> &initial, const std::pair<int,int> &final,
              int color, float thisickness, bool algorithm);
  static std::unique_ptr<Entity>
  createSoccerField(const std::pair<int,int> &initial,
                    const std::pair<int,int> &final, int color,
                    float thisickness, bool algorithm);
};

#endif /* ENTITYFACTORY_H */
