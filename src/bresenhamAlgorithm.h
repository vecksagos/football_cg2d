#ifndef BRESENHAMALGORITHM_H
#define BRESENHAMALGORITHM_H

#include <vector>
#include <utility>
#include "geometryAlgorithm.h"

class BresenhamAlgorithm : public GeometryAlgorithm {
public:
  ~BresenhamAlgorithm();
  std::vector<std::pair<int, int>>
    drawLine(const std::pair<int, int> &first,
             const std::pair<int, int> &second);
  std::vector<std::pair<int, int>>
    drawCircle(const std::pair<int, int> &center,
               const unsigned int radius);
  std::vector<std::pair<int, int>>
    drawRect(const std::pair<int, int> &first,
             const std::pair<int, int> &second);

private:
  std::vector<std::pair<int, int>>
    drawVerticalLine(const std::pair<int, int> &first,
             const std::pair<int, int> &second);
  std::vector<std::pair<int, int>>
    drawHorizontalLine(const std::pair<int, int> &first,
             const std::pair<int, int> &second);
};

#endif /* BRESENHAMALGORITHM_H */
