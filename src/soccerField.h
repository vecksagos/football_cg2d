/**
   @bug Só funciona quando o primeiro ponto é o superior esquerdo e o segundo
   é o inferior direito
*/
#ifndef SOCCERFIELD_H
#define SOCCERFIELD_H

#include "basicGeometrics.h"
#include <utility>
#include <memory>
#include "geometryAlgorithm.h"
#include <vector>

class SoccerField : public BasicGeometrics {
public:
  SoccerField(const std::pair<int,int> &first, const std::pair<int, int> &second,
              int color, float thickness, bool algorithm);
  virtual void load();
  virtual void update();

private:
  std::vector<std::vector<std::pair<int,int>>>
  horizontalField(const std::pair<int,int> &first,
                  const std::pair<int, int> &second,
                  std::unique_ptr<GeometryAlgorithm> alg);
  std::vector<std::vector<std::pair<int,int>>>
  verticalField(const std::pair<int,int> &first,
                  const std::pair<int, int> &second,
                  std::unique_ptr<GeometryAlgorithm> alg);
};

#endif /* SOCCERFIELD_H */
